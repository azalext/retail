# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

####################################################################

class SiteAdmin(admin.ModelAdmin):
    list_display  = ['name','fqdn','count','user','tools','sync','down','keep']
    list_filter   = ['sync','down','keep', 'user__username']
    #list_editable = ['name','fqdn']

    def tools(self, record):
        resp = {}

        resp['Open'] = "https://%(fqdn)s/"        % record.__dict__
        resp['View'] = "/retail/%(id)s/"        % record.__dict__
        resp['Sync'] = "/retail/%(id)s/refresh" % record.__dict__

        return "[ %s ]" % '&nbsp;|&nbsp;'.join(['<a href="%s">%s</a>' % (v,k) for k,v in resp.iteritems()])
    tools.allow_tags = True

    def count(self, record):
        return len(record.datasets.all())

admin.site.register(ShopifySite, SiteAdmin)

####################################################################

class DataAdmin(admin.ModelAdmin):
    #list_display  = ['name','fqdn','user']
    #list_filter   = ['user__username']
    ##list_editable = ['name','fqdn']

    def synchronize(self, record):
        pass

admin.site.register(ShopifyData, DataAdmin)

