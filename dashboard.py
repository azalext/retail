from controlcenter import Dashboard, widgets

from azalext.retail.models import *

################################################################################

class ShopifyList(widgets.ItemList):
    model = ShopifySite
    list_display = ('name', 'fqdn','user')

################################################################################

class MySingleBarChart(widgets.SingleBarChart):
    # label and series
    values_list = ('username', 'score')
    # Data source
    queryset = Person.objects.order_by('-score')
    limit_to = 3

################################################################################

class Landing(Dashboard):
    title = 'Retail'

    widgets = (
        ShopifyList,
        MySingleBarChart,
    )

