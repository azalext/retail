from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^$',                             views.homepage, name="site_list"),

    url(r'^(?P<uid>.+)/$',                 views.site_landing, name="site_view"),
    url(r'^(?P<uid>.+)/(?P<pid>[0-9]+)$',  views.site_product, name="site_item"),

    url(r'^(?P<uid>.+)/refresh/?$',        views.site_refresh, name="site_sync"),
    url(r'^(?P<uid>.+)/dataset$',          views.site_dataset, name="site_data"),

    url(r'^(?P<uid>.+)/modify$',           views.site_editing, name="site_edit"),
    url(r'^(?P<uid>.+)/delete$',           views.site_deletes, name="site_remv"),
]

