
# -*- coding: utf-8 -*-

from azalinc.node.models import *

################################################################################

class ShopifySite(models.Model):
    name = models.CharField(max_length=64)
    fqdn = models.CharField(max_length=256)

    user = models.ForeignKey(Person, related_name='sites', blank=True, null=True, default=None)
    last = models.ForeignKey('ShopifyData', blank=True, null=True, default=None)

    when = models.DateTimeField(auto_now=True)
    path = lambda self, *params: '/'.join([FTP_FOLDER, self.name]+list(params))

    keep = models.BooleanField(default=False, help_text="Keep all archives")
    sync = models.BooleanField(default=False, help_text="Auto synchronize")
    down = models.BooleanField(default=False, help_text="Download images")

    @property
    def curr_data(self):
        if not hasattr(self, '_data'):
            setattr(self,'_raw',[])

        if len(self._raw):
            try:
                self._raw = read_json(self.path('products.json'))
            except:
                pass

        return self._raw

    def save(self, *args, **kwargs):
        resp = super(ShopifySite, self).save(*args, **kwargs)

        if self.sync:
            from .tasks import shopping_download

            shopping_download.delay(self.pk)

        return resp

    def __str__(self): return str(self.name)

    class Meta:
        verbose_name = "Shopify Website"
        verbose_name_plural = "Shopify Websites"

#*******************************************************************************

class ShopifyData(models.Model):
    site = models.ForeignKey(ShopifySite, related_name='datasets')
    when = models.DateTimeField()

    name = property(lambda self: self.when.strftime('%Y-%m-%d_%H-%M-%S'))
    path = lambda self,*args,**kwargs: self.site.path(*args,**kwargs)

    keep = models.BooleanField(default=False, help_text="Keep images")
    sync = models.BooleanField(default=False, help_text="Auto synchronize")

    def save(self, *args, **kwargs):
        resp = super(ShopifyData, self).save(*args, **kwargs)

        #if not os.path.exists(self.path('covers')):
        #    os.system('mkdir -p %s' % self.path('covers'))

        return resp

    def __str__(self): return "%s" % (self.when)

    class Meta:
        verbose_name = "Shopify Payload"
        verbose_name_plural = "Shopify Payloads"

#*******************************************************************************

class ShopifyItem(models.Model):
    site = models.ForeignKey(ShopifySite, related_name='products')
    when = models.DateTimeField()

    name = property(lambda self: self.when.strftime('%Y-%m-%d_%H-%M-%S'))
    path = lambda self,*args,**kwargs: self.site.path(*args,**kwargs)

    name = models.CharField(max_length=128, help_text="Product Name")
    link = models.URLField(help_text="Product Link")

    def save(self, *args, **kwargs):
        resp = super(ShopifyData, self).save(*args, **kwargs)

        #if not os.path.exists(self.path('covers')):
        #    os.system('mkdir -p %s' % self.path('covers'))

        return resp

    def __str__(self): return "%s" % (self.when)

    class Meta:
        verbose_name = "Shopify Product"
        verbose_name_plural = "Shopify Products"

